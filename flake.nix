{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages.default = let
        source = pkgs.fetchFromGitHub {
          owner = "wvermin";
          repo = "findent";
          rev = "85c8225be6f526a47bc9ed6794a36ba7b522b90c";
          hash = "sha256-QGInRMX7aXP6mnmLvkPPiOnJ0ij8wx3+v2othozge7Q=";
        };
      in
        pkgs.stdenv.mkDerivation {
          pname = "findent";
          version = "4.2.1";

          # Ignore the Java front-end.
          src = "${source}/findent";

          meta = {
            homepage = "https://www.ratrabbit.nl/ratrabbit/findent/index.html";
            description = "Formatter for Fortran code.";
            license = pkgs.lib.licenses.bsd3;
          };
        };

      formatter = pkgs.alejandra;
    });
}
